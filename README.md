NEMO
====
Get Nemo
--------
To use last development state of Nemo, please clone the master branch.

To get sources please use these commands:

      git clone git@gitlab.inria.fr:memphis/nemo.git
